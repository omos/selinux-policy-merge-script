#!/bin/bash

if git rev-parse --verify CHERRY_PICK_HEAD &>/dev/null; then
	echo 1>&2 "ERROR: cherry-pick already in progress!"
	exit 1
fi

set -ex

mode="$1"; shift

if git cherry-pick "$@"; then
	exit 0
fi

# If git cherry-pick failed, but no cherry-pick in progress, then the user
# probably passed bad command-line options, so just exit non-zero.
if ! git rev-parse --verify CHERRY_PICK_HEAD &>/dev/null; then
	exit 1
fi

# cherry-pick failed - we have some conflicts!

while true; do
	case "$mode" in
	base)
		# Ignore changes on contrib side.
		git rm --ignore-unmatch policy/modules/contrib/*
		git reset HEAD policy/modules/contrib
		git restore policy/modules/contrib
		;;
	contrib)
		# Ignore changes outside contrib.
		git status --porcelain | awk '{if ($1=="DU") print $2}' | \
			git rm --pathspec-from-file -
		;;
	esac

	# If we still have conflicts, let the user resolve them.
	env="GIT_EDITOR=:"
	if [ "$(git diff --name-only --diff-filter=U | wc -l)" -ne 0 ]; then
		echo "Please resolve conflict, starting shell..."
		echo "Resolve all conflicts and exit the shell with 0."
		echo "If you exit non-zero, cherry-pick will be aborted."
		bash -i || { git cherry-pick --abort; exit 1; }
		# Let the user edit the commit message if manual conflict
		# resolution was needed.
		env=""
	fi

	# If nothing is staged after resolving, then just skip this commit
	# (probably some contrib-only change).
	if [ "$(git diff --name-only --cached | wc -l)" -ne 0 ]; then
		command="--continue"
	else
		command="--skip"
	fi
	# Try to continue cherry-picking; if it fails, continue resolving...
	if env $env git cherry-pick $command; then
		exit 0
	fi
done
