# Fedora SELinux policy merge scripts

This repo contains scripts to enable merging the two repos for Fedora SELinux policy - [base](https://github.com/fedora-selinux/selinux-policy) and [contrib](https://github.com/fedora-selinux/selinux-policy-contrib). This division into two repos is merely a historical artifact and just complicates working with the repos. So it's time to finally take a deep breath and finally merge it into one.

The approach taken by the script(s) here is:
1. \[contrib/rawhide\] Move all files to policy/modules/contrib directory.
2. \[base/rawhide\] Remove the contrib submodule.
3. Merge contrib/rawhide into the base rawhide branch. This needs careful merge conflict resolution, but that can be automated easily.

After the merge, only the base branch would be used; the corresponding contrib branch would be then archived and no longer used. The contrib repo's commit history would then be included in the base repo via the merge commit.

The plan is to only do this in rawhide and keep the stable branches split initially. New branches will already be branched from the merged rawhide branch, so eventually the contrib repo will no longer be used. Backporting from the merged branches to unmerged will need some conflict resolution in case of commits spanning both base and contrib, but that will be automated by helper scripts provided in this repo.

## Key files in this repo

### `merge-contrib.sh`

This is the script that does the magic of merging the base and contrib rawhide branches together.

To create the merged branch in your local git working copy:
1. `cd` to the root of the `selinux-policy` repo.
2. The script expects two remotes to be set up in the repo:
   * `origin` - pointing to https://github.com/fedora-selinux/selinux-policy
   * `contrib` - pointing to https://github.com/fedora-selinux/selinux-policy-contrib
3. Make sure there are no uncommited changes in the repo. You don't need to have a specific branch checked out, but be aware that the script will switch branches!
4. Run the `merge-contrib.sh` in the current directory.
5. It will fetch the current state of the remotes and create/overwrite two branches:
   * `contrib-rawhide` - a temporary working branch
   * `merge-contrib` - the final merge result

At the moment it works only with the `rawhide` branch, but support for stable/RHEL branches can be added easily in the future.

The script doesn't touch any other stuff in your repo and doesn't push anything anywhere, so don't be afraid to try it out! ;)

### `cherry-pick-merged-base.sh`

A helper script that helps with cherry-picking commits from a merged branch to an umerged base branch. It automacially resolves conflicts in base+contrib commits and filters out contrib-only ones. It accespts the same arguments as `git cherry-pick` and can handle multiple commits and/or commit ranges.

In case there is a usual merge conflict in file content, the script drops the user into a subshell to allow them to resolve the conflict manually. The user signals to the scripts that all conflicts are resolved by exiting the shell with 0 (i.e. by executing `exit 0`). The cherry-pick process is then resumed until all commits are processed or another merge conflict needs to be resolved. Alternatively, they can abort the cherry-pick by exiting with a non-zero exit code (equivalent to `git cherry-pick --abort`).

### `cherry-pick-merged-contrib.sh`

Equivalent to `cherry-pick-merged-base.sh`, but used for backporting into contrib branches.

## Adapting the dist-git files to work with a merged branch

After a contrib branch is merged into base, the corresponding dist-git branch will need to be adapted. The `make-rhat-patches.sh` script and the `selinux-policy.spec` file will need a small change to use only the base repo and work with only one selinux-policy tarball. There is a preliminary patch for this available here:

https://src.fedoraproject.org/fork/omos/rpms/selinux-policy/commits/contrib-merge

To test the above patch:
1. Apply it to the `master` branch.
2. Run the `make-rhat-patches.sh` script as follows (will create a tarball from my preview merged rawhide branch):
```bash
REPO_SELINUX_POLICY=https://github.com/WOnder93/selinux-policy \
	REPO_SELINUX_POLICY_BRANCH=merge-contrib \
	./make-rhat-patches.sh 
```
3. Do a scratch build from local SRPM (`fedpkg mock-build` or `fedpkg scratch-build --srpm`).

## Viewing commit history of contrib files in a merged branch

Naturally, the history of the non-contrib files will be viewable the same way as before, no tricks needed there. Only the contrib files will need some special handling to show also the pre-merge history.

### `git log policy/modules/contrib/<file>` use case

One will have to add the `--follow` command-line switch to see also the commits before the merge.

Example:
```
$ git log --oneline policy/modules/contrib/zabbix.te
0e1418dfb (contrib-rawhide) Move stuff around to match the main repo
1ec3d1a3a Remove files which are not part of refpolicy-contrib
c096ae5ad Move policies to contrib due to refpolicy
$ git log --oneline --follow policy/modules/contrib/zabbix.te
0e1418dfb (contrib-rawhide) Move stuff around to match the main repo
ab515a173 Allow zabbix_t manage and filetrans temporary socket files
8ce79b2c8 Allow zabbix_t domain to manage zabbix_var_lib_t sock files and connect to unix_stream_socket
45d56256d Allow zabbix_t domain to create sockets labeled as zabbix_var_run_t BZ(1683820)
d838576d5 Allow zabbix_agent_t domain to connect to redis_port_t
6c79d8452 Allow zabbix_agent_t to run zabbix scripts
ad11d09fd Fix typos in zabbix.te file
689fc25e5 Add missing requires
[...]
```

Unfortunately, the fil history feature of the GitHub web interface doesn't use `--follow` internally, so it won't display the full history. GitLab, however, shows the full history correctly.

### `git blame policy/modules/contrib/<file>` use case

This will work same as before even in GitHub interface, no additional command-line switches needed.
