#!/bin/bash

# Prerequisites:
# CWD == a repo with two remotes:
# * origin - pointed to https://github.com/fedora-selinux/selinux-policy
# * contrib - pointed to https://github.com/fedora-selinux/selinux-policy-contrib
# The repo must have no uncommitted changes.
# The script creates/overwrites two branches:
# * contrib-rawhide - a temporary working branch
# * merge-contrib - the final merge result

set -ex

dirname="$(dirname "$0")"

# Update remotes
git fetch --multiple origin contrib

# Swith to rawhide branch
git checkout origin/rawhide

# Purge untracked files
make clean

# Remove working branches if they exist
git branch -D merge-contrib contrib-rawhide || true

# Prepare contrib
git checkout -b contrib-rawhide contrib/rawhide

# Remove unwanted files
git rm .gitignore .travis.yml
# Move policy files to policy/modules/contrib
mkdir -p policy/modules/contrib
git mv *.te *.if *.fc Changelog metadata.xml policy/modules/contrib

git commit -m 'Move stuff around to match the main repo'

# Prepare main rawhide branch
git checkout -b merge-contrib origin/rawhide

# Remove the contrib subdirectory
git rm policy/modules/contrib .gitmodules
git commit -m 'Prepare to merge contrib repo'

# Now start the merge
git merge contrib-rawhide || true

# Take all files from the main branch, except policy/modules/contrib/
git reset HEAD '*'
git reset contrib-rawhide policy/modules/contrib/

# Finalize the merge
GIT_EDITOR=true git merge --continue
git commit --amend -m 'Merge contrib into the main repo'
# Cleanup uncommitted litter from the merge confilct resolution
git checkout '*'
rm -rf policy/modules/services
git checkout policy/modules/services

# Adapt .travis.yml
git am "$dirname/0001-Adapt-.travis.yml-to-contrib-merge.patch"
